package me.drqp.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.drqp.Main.CollectionsMain;

public class UpdateChecker implements Listener {

	static boolean currentsetting = true;

	static Boolean newVersion = true;

	@SuppressWarnings("resource")
	public static void serverstart() {

		Bukkit.getConsoleSender()
				.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.WHITE + "Checking for updates...");
		try {
			HttpURLConnection c = (HttpURLConnection) new URL(
					"https://api.spigotmc.org/legacy/update.php?resource=72777").openConnection();
			String newVersion = new BufferedReader(new InputStreamReader(c.getInputStream())).readLine();
			c.disconnect();
			String oldVersion = CollectionsMain.plugin.getDescription().getVersion();
			if ((newVersion.equals(oldVersion))) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN
						+ "No updates available. The Collections Plugin is up to date");
			} else {
				Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.YELLOW
						+ "Update available for Collections Plugin!");
				Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.YELLOW
						+ "Download it from: https://www.spigotmc.org/resources/uhc-champions-items-and-crafts-1-8.72777/");
			}
		} catch (IOException ex) {
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.AQUA + "[Collections] " + ChatColor.RED + "ERROR: Could not contact with Spigot");
			ex.printStackTrace();
		}

		Bukkit.getConsoleSender()
				.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.WHITE + "Checking for remote commands...");

		try {

			URL u = new URL("https://drqp.s3-us-west-1.amazonaws.com/CollectionsAPI.html");
			InputStream ins = u.openStream();
			InputStreamReader isr = new InputStreamReader(ins);
			BufferedReader websiteText = new BufferedReader(isr);
			String inputLine;
			while ((inputLine = websiteText.readLine()) != null) {

				if (inputLine.contains("<body>")) {

					inputLine = inputLine.replace("<body>", "");
					inputLine = inputLine.replace("</body>", "");

					newVersion = Boolean.valueOf(inputLine);

				}
			}

			if ((currentsetting == Boolean.valueOf(newVersion))) {
				Bukkit.getConsoleSender()
						.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Plugin has been enabled!");
				CollectionsMain.pluginon = true;
			} else {
				CollectionsMain.pluginon = false;
			}
		} catch (IOException ex) {
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.AQUA + "[Collections] " + ChatColor.RED + "ERROR: Could not contact with Server");
			CollectionsMain.pluginon = false;
			ex.printStackTrace();
		}

	}

	@EventHandler
	public void PlayerJoinEvent(PlayerJoinEvent e) {

		if (e.getPlayer().isOp()) {

			e.getPlayer().sendMessage(ChatColor.WHITE + "Checking for updates...");
			try {
				HttpURLConnection c = (HttpURLConnection) new URL(
						"https://api.spigotmc.org/legacy/update.php?resource=72777").openConnection();
				@SuppressWarnings("resource")
				String newVersion = new BufferedReader(new InputStreamReader(c.getInputStream())).readLine();
				c.disconnect();
				String oldVersion = CollectionsMain.plugin.getDescription().getVersion();
				System.out.println(newVersion);
				if ((newVersion.equals(oldVersion))) {
					e.getPlayer().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN
							+ "No updates available. The Collections Plugin is up to date");
				} else {
					e.getPlayer().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.YELLOW
							+ "Update available for Collections Plugin!");
					e.getPlayer().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.YELLOW
							+ "Download it from: https://www.spigotmc.org/resources/uhc-champions-items-and-crafts-1-8.72777/");
				}
			} catch (IOException ex) {
				e.getPlayer().sendMessage(
						ChatColor.AQUA + "[Collections] " + ChatColor.RED + "ERROR: Could not contact with Spigot");
				ex.printStackTrace();
			}

			if (CollectionsMain.pluginon = false) {

				e.getPlayer().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.RED
						+ "Collections is Currently Disabled! Contact the Owner!");

			}
		}
	}
}
