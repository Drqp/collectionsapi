package me.drqp.skills.GUI;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.drqp.skills.API.SkillsAPI;
import me.drqp.skills.Events.JoinEvent;

public class MainGUI implements Listener {

	public static void openGUI(Player p) {

		Inventory inv = Bukkit.createInventory(null, 18, ChatColor.GRAY + "Skills");

		ItemStack health = new ItemStack(Material.GOLDEN_APPLE);
		ItemMeta im1 = health.getItemMeta();
		im1.setDisplayName("�c�lHealth");
		im1.setLore(Arrays.asList("�7Current Health Skill Level: �f" + SkillsAPI.getHealthModifier(p),
				"�7Current Hearts: �f" + p.getHealth(), "", "�7Required Points: �61",
				"�7Current Skill Points: �6" + SkillsAPI.getPoints(p), "", "�7Click to upgrade!"));
		im1.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		health.setItemMeta(im1);

		ItemStack speed = new ItemStack(Material.FEATHER);
		ItemMeta im2 = speed.getItemMeta();
		im2.setDisplayName("�b�lSpeed");
		im2.setLore(Arrays.asList("�7Current Speed Skill Level: �f" + SkillsAPI.getWalkSpeedModifier(p),
				"�7Current Speed: �f" + p.getWalkSpeed(), "", "�7Required Points: �61",
				"�7Current Skill Points: �6" + SkillsAPI.getPoints(p), "", "�7Click to upgrade!"));
		im2.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		speed.setItemMeta(im2);

		ItemStack damage = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta im3 = damage.getItemMeta();
		im3.setDisplayName("�3�lDamage");
		im3.setLore(Arrays.asList("�7Current Damage Skill Level: �f" + SkillsAPI.getDamageModifier(p),
				"�7Current Damage Modifier: �f" + SkillsAPI.getDamageModifier(p), "", "�7Required Points: �61",
				"�7Current Skill Points: �6" + SkillsAPI.getPoints(p), "", "�7Click to upgrade! "));
		im3.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im3.addItemFlags(ItemFlag.HIDE_ENCHANTS);

		damage.setItemMeta(im3);

		ItemStack mining = new ItemStack(Material.DIAMOND_PICKAXE);
		ItemMeta im4 = damage.getItemMeta();
		im4.setDisplayName("�6�lMining");
		im4.setLore(Arrays.asList("�7Current Damage Skill Level: �f" + SkillsAPI.getMiningModifier(p),
				"�7Current Mining Modifier: �f" + SkillsAPI.getMiningModifier(p), "", "�7Required Points: �61",
				"�7Current Skill Points: �6" + SkillsAPI.getPoints(p), "", "�7Click to upgrade! "));
		im4.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im4.addItemFlags(ItemFlag.HIDE_ENCHANTS);

		mining.setItemMeta(im4);

		for (int i = 0; i < inv.getSize(); i++) {

			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
			ItemMeta im = is.getItemMeta();

			im.setDisplayName("�7");

			is.setItemMeta(im);

			inv.setItem(i, is);
		}

		inv.setItem(3, health);
		inv.setItem(4, speed);
		inv.setItem(5, damage);
		inv.setItem(13, mining);

		p.openInventory(inv);

	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {

		if (!ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Skills"))
			return;
		if (e.getCurrentItem() == null)
			return;
		if (e.getCurrentItem().getItemMeta() == null)
			return;

		Player p = (Player) e.getWhoClicked();

		if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Health")) {

			if (SkillsAPI.getPoints(p) >= 1) {

				if (SkillsAPI.getHealthModifier(p) == 5) {

					p.sendMessage("�cMaxed Out!");
					e.setCancelled(true);

				} else {

					SkillsAPI.setHealthModifier(p, SkillsAPI.getHealthModifier(p) + 1);
					SkillsAPI.subtractPoints(p, 1);

					e.setCancelled(true);
					openGUI(p);

					p.sendMessage("�aSuccessfully Upgraded!");

					JoinEvent.setup(p);
				}
			} else {

				e.setCancelled(true);
				p.closeInventory();
				p.sendMessage("�cInsufficient Skill Points!");
			}

		}

		if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Speed")) {

			if (SkillsAPI.getPoints(p) >= 1) {

				if (SkillsAPI.getWalkSpeedModifier(p) == 5) {

					p.sendMessage("�cMaxed Out!");
					e.setCancelled(true);

				} else {

					SkillsAPI.setWalkSpeedModifier(p, SkillsAPI.getWalkSpeedModifier(p) + 1);
					SkillsAPI.subtractPoints(p, 1);

					e.setCancelled(true);
					openGUI(p);

					p.sendMessage("�aSuccessfully Upgraded!");

					JoinEvent.setup(p);
				}
			} else {

				e.setCancelled(true);
				p.closeInventory();
				p.sendMessage("�cInsufficient Skill Points!");
			}

		}

		if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Damage")) {

			if (SkillsAPI.getPoints(p) >= 1) {

				if (SkillsAPI.getDamageModifier(p) == 5) {

					p.sendMessage("�cMaxed Out!");
					e.setCancelled(true);

				} else {

					SkillsAPI.setDamageModifier(p, SkillsAPI.getDamageModifier(p) + 1);
					SkillsAPI.subtractPoints(p, 1);

					e.setCancelled(true);
					openGUI(p);

					p.sendMessage("�aSuccessfully Upgraded!");

					JoinEvent.setup(p);
				}
			} else {

				e.setCancelled(true);
				p.closeInventory();
				p.sendMessage("�cInsufficient Skill Points!");
			}

		}

		if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Mining")) {

			if ((SkillsAPI.getPoints(p)) >= 1) {

				if (SkillsAPI.getMiningModifier(p) == 5) {

					p.sendMessage("�cMaxed Out!");
					e.setCancelled(true);

				} else {

					SkillsAPI.setMiningModifier(p, SkillsAPI.getMiningModifier(p) + 1);

					SkillsAPI.subtractPoints(p, 1);
					e.setCancelled(true);
					openGUI(p);

					p.sendMessage("�aSuccessfully Upgraded!");

					JoinEvent.setup(p);
				}
			} else {

				e.setCancelled(true);
				p.closeInventory();
				p.sendMessage("�cInsufficient Skill Points!");
			}

		}
	}

}
