package me.drqp.skills.API;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class SkillsAPI {

	public static HashMap<UUID, Integer> skillpoints = new HashMap<>();
	public static HashMap<UUID, Integer> damagemodifier = new HashMap<>();
	public static HashMap<UUID, Integer> healthmodifier = new HashMap<>();
	public static HashMap<UUID, Integer> speedmodifier = new HashMap<>();
	public static HashMap<UUID, Integer> miningmodifier = new HashMap<>();

	public static int getHealthModifier(Player p) {

		int num = healthmodifier.get(p.getUniqueId());

		return num;
	}

	public static int getWalkSpeedModifier(Player p) {

		int num = speedmodifier.get(p.getUniqueId());

		return num;
	}

	public static int getDamageModifier(Player p) {

		int num = damagemodifier.get(p.getUniqueId());

		return num;
	}

	public static int getMiningModifier(Player p) {

		int num = miningmodifier.get(p.getUniqueId());

		return num;
	}

	public static void setHealthModifier(Player p, Integer onetofive) {

		healthmodifier.put(p.getUniqueId(), onetofive);

		return;

	}

	public static void setWalkSpeedModifier(Player p, Integer onetofive) {

		speedmodifier.put(p.getUniqueId(), onetofive);

		return;
	}

	public static void setDamageModifier(Player p, Integer onetofive) {

		damagemodifier.put(p.getUniqueId(), onetofive);

		return;
	}

	public static void setMiningModifier(Player p, Integer onetofive) {

		miningmodifier.put(p.getUniqueId(), onetofive);

		return;
	}

	public static Integer getPoints(Player p) {

		int points = skillpoints.get(p.getUniqueId());

		return points;

	}

	public static void addPoints(Player p, Integer v) {

		skillpoints.put(p.getUniqueId(), getPoints(p) + v);

		p.sendMessage("");
		p.sendMessage("�6 You have earned " + v + " skill point!");
		p.sendMessage("");

		return;

	}

	public static void subtractPoints(Player p, Integer v) {

		skillpoints.put(p.getUniqueId(), getPoints(p) - v);

		return;

	}

	public static void setPoints(Player p, Integer v) {

		skillpoints.put(p.getUniqueId(), v);

		return;

	}

	public static void registerPoints(Player p) {

		if (skillpoints.get(p.getUniqueId()) == null) {

			skillpoints.put(p.getUniqueId(), 0);
		}

		if (healthmodifier.get(p.getUniqueId()) == null) {

			healthmodifier.put(p.getUniqueId(), 0);

		}

		if (damagemodifier.get(p.getUniqueId()) == null) {

			damagemodifier.put(p.getUniqueId(), 0);

		}

		if (speedmodifier.get(p.getUniqueId()) == null) {

			speedmodifier.put(p.getUniqueId(), 0);

		}

		if (miningmodifier.get(p.getUniqueId()) == null) {

			miningmodifier.put(p.getUniqueId(), 0);

		}

		return;

	}

	public static void forceRegisterPoints(Player p) {

		skillpoints.put(p.getUniqueId(), 0);

		healthmodifier.put(p.getUniqueId(), 0);

		damagemodifier.put(p.getUniqueId(), 0);

		speedmodifier.put(p.getUniqueId(), 0);

		miningmodifier.put(p.getUniqueId(), 0);

		p.sendMessage("�6�lAll Of Your Points Have Been Force Resetted!");

		return;

	}

}
