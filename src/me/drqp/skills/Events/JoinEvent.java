package me.drqp.skills.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.drqp.skills.API.SkillsAPI;

public class JoinEvent implements Listener {

	@EventHandler
	public void onjoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();

		setup(p);

	}

	public static void setup(Player p) {
		if (SkillsAPI.getHealthModifier(p) == 1) {

			p.setMaxHealth(20);

			p.setMaxHealth(p.getHealth() + 2);

		}

		if (SkillsAPI.getHealthModifier(p) == 2) {

			p.setMaxHealth(20);

			p.setMaxHealth(p.getHealth() + 4);

		}

		if (SkillsAPI.getHealthModifier(p) == 3) {

			p.setMaxHealth(p.getHealth() + 6);

		}

		if (SkillsAPI.getHealthModifier(p) == 4) {

			p.setMaxHealth(20);

			p.setMaxHealth(p.getHealth() + 8);

		}

		if (SkillsAPI.getHealthModifier(p) == 5) {

			p.setMaxHealth(20);

			p.setMaxHealth(p.getHealth() + 10);

		}

		if (SkillsAPI.getHealthModifier(p) == 0) {

			p.setMaxHealth(20);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 1) {

			p.setWalkSpeed(p.getWalkSpeed());

			float newspeed = (float) (p.getWalkSpeed() + 0.025);

			p.setWalkSpeed(newspeed);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 2) {

			p.setWalkSpeed((float) 0.2);

			float newspeed = (float) (p.getWalkSpeed() + 0.050);

			p.setWalkSpeed(newspeed);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 3) {

			p.setWalkSpeed((float) 0.2);

			float newspeed = (float) (p.getWalkSpeed() + 0.075);

			p.setWalkSpeed(newspeed);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 4) {

			p.setWalkSpeed((float) 0.2);

			float newspeed = (float) (p.getWalkSpeed() + 0.1);

			p.setWalkSpeed(newspeed);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 5) {

			p.setWalkSpeed((float) 0.2);

			float newspeed = (float) (p.getWalkSpeed() + 0.125);

			p.setWalkSpeed(newspeed);

		}

		if (SkillsAPI.getWalkSpeedModifier(p) == 0) {

			p.setWalkSpeed((float) 0.2);

		}

		if (SkillsAPI.getMiningModifier(p) == 1) {

			p.removePotionEffect(PotionEffectType.FAST_DIGGING);

			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 10000000, 0));

		}

		if (SkillsAPI.getMiningModifier(p) == 2) {

			p.removePotionEffect(PotionEffectType.FAST_DIGGING);

			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 10000000, 1));

		}

		if (SkillsAPI.getMiningModifier(p) == 3) {

			p.removePotionEffect(PotionEffectType.FAST_DIGGING);

			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 10000000, 2));

		}

		if (SkillsAPI.getMiningModifier(p) == 4) {

			p.removePotionEffect(PotionEffectType.FAST_DIGGING);

			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 10000000, 3));

		}

		if (SkillsAPI.getMiningModifier(p) == 5) {

			p.removePotionEffect(PotionEffectType.FAST_DIGGING);

			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 10000000, 4));

		}

	}

}
