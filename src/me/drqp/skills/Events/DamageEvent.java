package me.drqp.skills.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.drqp.skills.API.SkillsAPI;

public class DamageEvent implements Listener {

	@EventHandler
	public void damagevent(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player) {

			Player p = (Player) e.getDamager();

			if (SkillsAPI.getDamageModifier(p) == 1) {

				e.setDamage(e.getDamage() + 1);

			}

			if (SkillsAPI.getDamageModifier(p) == 2) {

				e.setDamage(e.getDamage() + 2);

			}

			if (SkillsAPI.getDamageModifier(p) == 3) {

				e.setDamage(e.getDamage() + 3);

			}

			if (SkillsAPI.getDamageModifier(p) == 4) {

				e.setDamage(e.getDamage() + 4);

			}

			if (SkillsAPI.getDamageModifier(p) == 5) {

				e.setDamage(e.getDamage() + 5);

			}

		}

	}

}
