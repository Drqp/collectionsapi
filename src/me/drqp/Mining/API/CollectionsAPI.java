package me.drqp.Mining.API;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import me.drqp.Main.CollectionsMain;

public class CollectionsAPI {

	public static Table<UUID, Material, Integer> blocksMined = HashBasedTable.create();

	public static Table<UUID, Material, Integer> blocklevel = HashBasedTable.create();

	public static ArrayList<Material> whitelistedblocks = new ArrayList<>();

	public static HashMap<UUID, Boolean> registered = new HashMap<>();

	public static ArrayList<Material> compactblocks = new ArrayList<>();

	public static Integer getBlocksMined(Player p, Material material) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMined.row(uuid);

		int blocksmined = blocksmap.get(material);

		return blocksmined;
	}

	public static void addBlocks(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMined.row(uuid);

		blocksmap.put(material, blocksmap.get(material) + value);

		return;

	}

	public static void removeBlocks(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMined.row(uuid);

		blocksmap.put(material, blocksmap.get(material) - value);

		return;
	}

	public static void setupBlocks(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMined.row(uuid);

		blocksmap.put(material, value);

		return;
	}

	public static void addBlockWhiteList(Material material) {

		whitelistedblocks.add(material);

		return;
	}

	public static void setLevel(Player p, Material material, Integer value) {

		Map<Material, Integer> level = blocklevel.row(p.getUniqueId());

		level.put(material, value);

		return;
	}

	public static Integer getLevel(Player p, Material material) {

		Map<Material, Integer> level = blocklevel.row(p.getUniqueId());

		int levelint = level.get(material);

		return levelint;
	}

	public static void setupLevels(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> level = blocklevel.row(uuid);

		level.put(material, value);

		return;
	}

	public static Integer getBlocksMinedRequiredForLevel(Player p, Material material) {

		FileConfiguration config = CollectionsMain.plugin.getConfig();

		int blocksrequired = 0;

		if (CollectionsAPI.getLevel(p, material) < 1) {

			blocksrequired = config.getInt("level1");

		}

		if (CollectionsAPI.getLevel(p, material) == 1) {

			blocksrequired = config.getInt("level2");

		}

		if (CollectionsAPI.getLevel(p, material) == 2) {

			blocksrequired = config.getInt("level3");

		}

		if (CollectionsAPI.getLevel(p, material) == 3) {

			blocksrequired = config.getInt("level4");

		}

		if (CollectionsAPI.getLevel(p, material) == 4) {

			blocksrequired = config.getInt("level5");

		}

		if (CollectionsAPI.getLevel(p, material) == 5) {

			blocksrequired = config.getInt("level6");

		}

		if (CollectionsAPI.getLevel(p, material) == 6) {

			blocksrequired = config.getInt("level7");

		}

		if (CollectionsAPI.getLevel(p, material) == 7) {

			blocksrequired = config.getInt("level8");

		}

		if (CollectionsAPI.getLevel(p, material) == 8) {

			blocksrequired = config.getInt("level9");

		}

		return blocksrequired;

	}

	public static void registerPlayer(Player p) {

		Map<Material, Integer> blocksmap = blocksMined.row(p.getUniqueId());
		Map<Material, Integer> level = blocklevel.row(p.getUniqueId());

		for (Material m : whitelistedblocks) {

			if (blocksmap.get(m) == null) {
				setupBlocks(p, m, 0);
			}

			if (level.get(m) == null) {
				setupLevels(p, m, 0);
			}

		}

	}

	public static void forceRegisterPlayer(Player p) {

		p.sendMessage("�3�lAll Of Your Collections Have Been Force Resetted!");

		for (Material m : whitelistedblocks) {

			setupBlocks(p, m, 0);
			setupLevels(p, m, 0);

		}

	}

	public static void setupCompact() {

		compactblocks.add(Material.IRON_BLOCK);
		compactblocks.add(Material.GOLD_BLOCK);
		compactblocks.add(Material.DIAMOND_BLOCK);
		compactblocks.add(Material.EMERALD_BLOCK);
		compactblocks.add(Material.COAL_BLOCK);
		compactblocks.add(Material.LAPIS_BLOCK);

	}

	public static void collectionsdisable() {

	}
}
