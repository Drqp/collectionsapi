package me.drqp.Mining.Events;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.connorlinfoot.actionbarapi.ActionBarAPI;

import me.drqp.Main.CollectionsMain;
import me.drqp.Mining.API.CollectionsAPI;
import me.drqp.skills.API.SkillsAPI;

public class BlockBreak implements Listener {

	FileConfiguration config = CollectionsMain.plugin.getConfig();

	@EventHandler
	public void breakevent(BlockBreakEvent e) {

		Player p = e.getPlayer();

		if (CollectionsAPI.compactblocks.contains(e.getBlock().getType())) {

			String materialm = e.getBlock().getType().toString();
			materialm = materialm.replace("_BLOCK", "_ORE");

			Material m = Material.valueOf(materialm);

			CollectionsAPI.addBlocks(p, m, 9);

			String material = materialm;
			material = material.replace("_", "žb ");
			material = material.replace(" ORE", "žb");

			if (CollectionsAPI.getLevel(p, m) == 9) {

				ActionBarAPI.sendActionBar(p, "žbCollection Updated! +9 (" + CollectionsAPI.getBlocksMined(p, m) + "/"
						+ "Unlimited) (" + material + ")");
			} else {

				ActionBarAPI.sendActionBar(p, "žbCollection Updated! +9 (" + CollectionsAPI.getBlocksMined(p, m) + "/"
						+ CollectionsAPI.getBlocksMinedRequiredForLevel(p, m) + ") (" + material + ")");
			}

			p.playSound(p.getLocation(), Sound.LAVA_POP, 10, 29);

		} else {

			if (CollectionsAPI.whitelistedblocks.contains(e.getBlock().getType())) {
				if (p.getGameMode() == GameMode.SURVIVAL) {

					CollectionsAPI.addBlocks(p, e.getBlock().getType(), 1);

					String material = e.getBlock().getType().toString();
					material = material.replace("_", "žb ");
					material = material.replace(" ORE", "žb");

					if (CollectionsAPI.getLevel(p, e.getBlock().getType()) == 9) {

						ActionBarAPI.sendActionBar(p,
								"žbCollection Updated! +1 (" + CollectionsAPI.getBlocksMined(p, e.getBlock().getType())
										+ "/" + "Unlimited) (" + material + ")");
					} else {

						ActionBarAPI.sendActionBar(p,
								"žbCollection Updated! +1 (" + CollectionsAPI.getBlocksMined(p, e.getBlock().getType())
										+ "/" + CollectionsAPI.getBlocksMinedRequiredForLevel(p, e.getBlock().getType())
										+ ") (" + material + ")");
					}

					p.playSound(p.getLocation(), Sound.LAVA_POP, 10, 29);
				}

			}
		}

	}

	@EventHandler
	public void levelupevent(BlockBreakEvent e) {

		String materialm = e.getBlock().getType().toString();
		materialm = materialm.replace("_BLOCK", "_ORE");

		Material material = Material.valueOf(materialm);

		Player p = e.getPlayer();

		String materialraw = materialm;
		materialraw = materialraw.replace("_", "žb ");
		materialraw = materialraw.replace(" ORE", "žb");

		if (!CollectionsAPI.compactblocks.contains(e.getBlock().getType())) {

			if (CollectionsAPI.whitelistedblocks.contains(material)) {

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == 1) {

					e.getPlayer().sendMessage("žbNew Collection Unlocked! (" + materialraw + ")");

				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level1")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level2")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");

					SkillsAPI.addPoints(p, 1);
				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level3")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");
				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level4")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");
				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level5")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");

					SkillsAPI.addPoints(p, 1);

				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level6")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");
				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level7")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");
				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level8")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have increased your " + materialraw
									+ " collection level by 1. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");

				}

				if (CollectionsAPI.getBlocksMined(e.getPlayer(), material) == config.getInt("level9")) {

					CollectionsAPI.setLevel(e.getPlayer(), material,
							CollectionsAPI.getLevel(e.getPlayer(), material) + 1);

					e.getPlayer()
							.sendMessage("žbCongratulations! You have completed the " + materialraw
									+ " collection. Current Level is now "
									+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");

					SkillsAPI.addPoints(p, 1);

				}
			}

		} else {

			int one = CollectionsAPI.getBlocksMinedRequiredForLevel(p, material);
			int two = CollectionsAPI.getBlocksMinedRequiredForLevel(p, material) - 9;

			if (CollectionsAPI.getBlocksMined(p, material) > two && CollectionsAPI.getBlocksMined(p, material) <= one) {

				CollectionsAPI.setLevel(p, material, CollectionsAPI.getLevel(p, material) + 1);
				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ CollectionsAPI.getLevel(e.getPlayer(), material) + ".");

				if (CollectionsAPI.getLevel(p, material) == 3 || CollectionsAPI.getLevel(p, material) == 6
						|| CollectionsAPI.getLevel(p, material) == 9) {

					SkillsAPI.addPoints(p, 1);

				}

			}

		}

	}

}
