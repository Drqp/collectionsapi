package me.drqp.Mining.GUI;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.drqp.Mining.API.CollectionsAPI;
import me.drqp.Utils.ProgressBar;

public class CollectionsGUI implements Listener {

	public static Inventory inv = Bukkit.createInventory(null, 18, ChatColor.GRAY + "Collections");

	public static void openCollectionsGUI(Player p) {

		int i = -1;

		for (Material m : CollectionsAPI.whitelistedblocks) {

			if (i < inv.getSize()) {

				ItemStack is = new ItemStack(m);

				ItemMeta im = is.getItemMeta();

				String materialm = m.toString();

				materialm = materialm.replace("_ORE", "");

				materialm = materialm.toLowerCase();

				materialm = materialm.substring(0, 1).toUpperCase() + materialm.substring(1);

				materialm = "�b�l" + materialm + " Collection";

				im.setDisplayName(materialm);

				if (CollectionsAPI.getBlocksMined(p, m) < 1) {

					im.setLore(Arrays.asList("�7", "�cThis Collection Is Locked", ""));
					is.setType(Material.STONE);

				} else {

					if (CollectionsAPI.getLevel(p, m) >= 9) {

						String s = "�7Blocks Required For Next Level: �aCollection Completed";

						im.setLore(Arrays.asList("�7", "�aCollection Unlocked", "�7", "�7Statistics: ",
								"�7Current Blocks Mined: �b" + CollectionsAPI.getBlocksMined(p, m), s,
								"�7Current Level: �b" + CollectionsAPI.getLevel(p, m) + " "));

					} else {

						String s = "�7Blocks Required For Next Level: �b"
								+ CollectionsAPI.getBlocksMinedRequiredForLevel(p, m);

						int percent = CollectionsAPI.getBlocksMined(p, m) * 100
								/ CollectionsAPI.getBlocksMinedRequiredForLevel(p, m);
						double b = Math.round(percent * 10.0) / 10.0;

						String bar = "�7Your Progress: �8[�r"
								+ ProgressBar.getProgressBar(CollectionsAPI.getBlocksMined(p, m),
										CollectionsAPI.getBlocksMinedRequiredForLevel(p, m), 40, "|", "�b", "�7")
								+ "�8]";

						im.setLore(Arrays.asList("�7", "�aCollection Unlocked", "�7", "�7Statistics: ",
								"�7Current Blocks Mined: �b" + CollectionsAPI.getBlocksMined(p, m), s,
								"�7Current Level: �b" + CollectionsAPI.getLevel(p, m), "�b", bar,
								"�7Percentage Completed: �b" + percent + "%", ""));

					}

				}

				i++;

				is.setItemMeta(im);

				inv.setItem(i, is);
			}
		}

		p.openInventory(inv);

	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {

		if (!ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Collections"))
			return;
		if (e.getCurrentItem() == null)
			return;
		if (e.getCurrentItem().getItemMeta() == null)
			return;

		e.setCancelled(true);
	}

}
