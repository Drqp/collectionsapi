package me.drqp.Mining.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.drqp.Mining.GUI.CollectionsGUI;

public class OpenCollectionsGUI implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {

		if (cmd.getName().equalsIgnoreCase("collections")) {

			CollectionsGUI.openCollectionsGUI((Player) sender);

		}
		return false;
	}

}
