package me.drqp.Mining.Commands;

import java.util.InputMismatchException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.drqp.Mining.API.CollectionsAPI;
import me.drqp.skills.API.SkillsAPI;
import me.drqp.skills.Events.JoinEvent;
import me.drqp.skills.GUI.MainGUI;
import net.md_5.bungee.api.ChatColor;

public class OpenSkillGUI implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {

		if (cmd.getName().equalsIgnoreCase("skills")) {

			if (args.length == 0) {

				MainGUI.openGUI((Player) sender);

			} else if (sender.hasPermission("skills.admin")) {

				switch (args[0]) {

				case "setpoints":

					try {

						Player p = Bukkit.getPlayerExact(args[1]);

						if (args.length == 3) {

							try {

								int i = Integer.valueOf(args[2]);

								SkillsAPI.setPoints(p, i);
								sender.sendMessage("�aSuccessful!");
								p.sendMessage("�aYou have been given �6" + i + " Skill Point(s).");

							} catch (InputMismatchException e) {

								sender.sendMessage("�cNot a valid number!");

							}

						} else {

						}

					} catch (NullPointerException e) {
						sender.sendMessage(ChatColor.RED + "Player does not exist!");

					}

					break;

				case "setlevel":

					try {

						Player p = Bukkit.getPlayerExact(args[1]);

						if (args.length == 4) {

							try {
								int i = Integer.valueOf(args[3]);

								if (i <= 5) {

									switch (args[2]) {

									case "health":

										SkillsAPI.setHealthModifier(p, i);

										p.sendMessage("�aHealth Skill Succesfully Changed to " + i);

										JoinEvent.setup(p);

										break;

									case "speed":

										SkillsAPI.setWalkSpeedModifier(p, i);

										p.sendMessage("�aSpeed Skill Succesfully Changed to " + i);

										JoinEvent.setup(p);

										break;

									case "damage":

										SkillsAPI.setDamageModifier(p, i);
										p.sendMessage("�aDamage Skill Succesfully Changed to " + i);

										JoinEvent.setup(p);

										break;

									case "mining":

										SkillsAPI.setMiningModifier(p, i);
										p.sendMessage("�aMining Skill Succesfully Changed to " + i);

										JoinEvent.setup(p);

										break;

									}
								}

							} catch (InputMismatchException e) {

								sender.sendMessage("�cNot a valid number!");

							}

						} else {
							sender.sendMessage(ChatColor.RED + "�cSkills Commands:");
							sender.sendMessage(ChatColor.RED
									+ "�c - /skills setlevel <player> <health/mining/speed/damage> <level (1-5)>");
							sender.sendMessage(ChatColor.RED + "�c - /skills setpoints <player> <points>");
						}
					} catch (NullPointerException e) {

						sender.sendMessage(ChatColor.RED + "Player does not exist!");

					}
					break;

				case "resetall":

					Player p = (Player) sender;
					if (p.hasPermission("skills.resetall")) {

						p.sendMessage("�4Skills have been reset for: ");

						for (Player player : Bukkit.getOnlinePlayers()) {

							SkillsAPI.forceRegisterPoints(player);

							CollectionsAPI.forceRegisterPlayer(player);

							p.sendMessage(" - " + player);

						}

					}

				default:
					sender.sendMessage(ChatColor.RED
							+ "�c - /skills setlevel <player> <health/mining/speed/damage> <level (1-5)>");
					sender.sendMessage(ChatColor.RED + "�c - /skills setpoints <player> <points>");
					break;
				}

			}

		}
		// TODO Auto-generated method stub
		return false;
	}

}
