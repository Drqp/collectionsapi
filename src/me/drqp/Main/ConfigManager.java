package me.drqp.Main;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {

	private CollectionsMain plugin = CollectionsMain.getPlugin(CollectionsMain.class);

	public FileConfiguration miningcfg;
	public File miningfile;

	public void setup() {

		if (plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}

		miningfile = new File(plugin.getDataFolder(), "mining.yml");

		if (!miningfile.exists()) {
			try {
				miningfile.createNewFile();
			} catch (IOException e) {
				System.out.print("[Collections] Could not create mining.yml!");
			}
		}

		miningcfg = YamlConfiguration.loadConfiguration(miningfile);
		Bukkit.getConsoleSender()
				.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Succesfully generated mining.yml");
	}

	public FileConfiguration getmining() {
		return miningcfg;
	}

	public void savemining() {
		try {
			miningcfg.save(miningfile);
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Save Failed!");
		}
	}

	public void reloadmining() {
		miningcfg = YamlConfiguration.loadConfiguration(miningfile);
	}

}
