package me.drqp.Mining.Events;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.drqp.Main.ConfigManager;
import me.drqp.Mining.API.CollectionsAPI;
import me.drqp.skills.API.SkillsAPI;
import net.md_5.bungee.api.ChatColor;

public class InitialSetup implements Listener {

	public static ArrayList<UUID> players = new ArrayList<>();

	public static ArrayList<Material> defaultblocks = new ArrayList<>();

	public static boolean collections = true;

	private static ConfigManager cfgm;

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {

		if (collections == true) {

			CollectionsAPI.registerPlayer(e.getPlayer());
			SkillsAPI.registerPoints(e.getPlayer());
		}

		else {

			if (e.getPlayer().isOp()) {

				e.getPlayer().sendMessage(ChatColor.RED + "Warning! Collections are currently disabled!");

			}
		}

	}

	public static void setup() {

		defaultblocks.add(Material.STONE);
		defaultblocks.add(Material.COAL_ORE);
		defaultblocks.add(Material.IRON_ORE);
		defaultblocks.add(Material.GOLD_ORE);
		defaultblocks.add(Material.DIAMOND_ORE);
		defaultblocks.add(Material.LAPIS_ORE);
		defaultblocks.add(Material.EMERALD_ORE);
		defaultblocks.add(Material.REDSTONE_ORE);
		defaultblocks.add(Material.QUARTZ_BLOCK);
		defaultblocks.add(Material.OBSIDIAN);
		defaultblocks.add(Material.GLOWSTONE);
		defaultblocks.add(Material.GRAVEL);
		defaultblocks.add(Material.ICE);
		defaultblocks.add(Material.SAND);
		defaultblocks.add(Material.NETHERRACK);

		for (Material m : defaultblocks) {

			if (!CollectionsAPI.whitelistedblocks.contains(m)) {

				CollectionsAPI.addBlockWhiteList(m);

			}
		}
		CollectionsAPI.setupCompact();

	}

}