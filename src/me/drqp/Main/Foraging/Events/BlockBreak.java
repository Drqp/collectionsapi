package me.drqp.Main.Foraging.Events;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.connorlinfoot.actionbarapi.ActionBarAPI;

import me.drqp.Main.CollectionsMain;
import me.drqp.Main.Foraging.API.ForagingAPI;
import me.drqp.skills.API.SkillsAPI;

public class BlockBreak implements Listener {

	FileConfiguration config = CollectionsMain.plugin.getConfig();

	@EventHandler
	public void breakevent(BlockBreakEvent e) {

		Player p = e.getPlayer();

		if (ForagingAPI.whitelistedblocksf.contains(e.getBlock().getType())) {
			if (p.getGameMode() == GameMode.SURVIVAL) {

				ForagingAPI.addBlocksF(p, e.getBlock().getType(), 1);

				String material = e.getBlock().getType().toString();
				material = material.replace("_", "žb ");
				material = material.replace(" ORE", "žb");

				if (ForagingAPI.getLevelF(p, e.getBlock().getType()) == 9) {

					ActionBarAPI.sendActionBar(p,
							"žbCollection Updated! +1 (" + ForagingAPI.getblocksMinedF(p, e.getBlock().getType()) + "/"
									+ "Unlimited) (" + material + ")");
				} else {

					ActionBarAPI.sendActionBar(p,
							"žbCollection Updated! +1 (" + ForagingAPI.getblocksMinedF(p, e.getBlock().getType()) + "/"
									+ ForagingAPI.getblocksMinedFRequiredForLevelF(p, e.getBlock().getType()) + ") ("
									+ material + ")");
				}

				p.playSound(p.getLocation(), Sound.LAVA_POP, 10, 29);
			}

		}

	}

	@EventHandler
	public void levelupevent(BlockBreakEvent e) {

		String materialm = e.getBlock().getType().toString();
		materialm = materialm.replace("_BLOCK", "_ORE");

		Material material = Material.valueOf(materialm);

		Player p = e.getPlayer();

		String materialraw = materialm;
		materialraw = materialraw.replace("_", "žb ");
		materialraw = materialraw.replace(" ORE", "žb");

		if (ForagingAPI.whitelistedblocksf.contains(material)) {

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == 1) {

				e.getPlayer().sendMessage("žbNew Collection Unlocked! (" + materialraw + ")");

			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level1")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level2")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");

				SkillsAPI.addPoints(p, 1);
			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level3")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");
			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level4")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");
			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level5")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");

				SkillsAPI.addPoints(p, 1);

			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level6")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");
			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level7")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");
			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level8")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer()
						.sendMessage("žbCongratulations! You have increased your " + materialraw
								+ " collection level by 1. Current Level is now "
								+ ForagingAPI.getLevelF(e.getPlayer(), material) + ".");

			}

			if (ForagingAPI.getblocksMinedF(e.getPlayer(), material) == config.getInt("level9")) {

				ForagingAPI.setLevelF(e.getPlayer(), material, ForagingAPI.getLevelF(e.getPlayer(), material) + 1);

				e.getPlayer().sendMessage("žbCongratulations! You have completed the " + materialraw
						+ " collection. Current Level is now " + ForagingAPI.getLevelF(e.getPlayer(), material) + ".");

				SkillsAPI.addPoints(p, 1);

			}
		}

	}

}
