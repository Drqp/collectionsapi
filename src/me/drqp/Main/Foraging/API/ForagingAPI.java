package me.drqp.Main.Foraging.API;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import me.drqp.Main.CollectionsMain;

public class ForagingAPI {

	public static Table<UUID, Material, Integer> blocksMinedF = HashBasedTable.create();

	public static Table<UUID, Material, Integer> blockslevelF = HashBasedTable.create();

	public static ArrayList<Material> whitelistedblocksf = new ArrayList<>();

	public static HashMap<UUID, Boolean> registeredf = new HashMap<>();

	public static Integer getblocksMinedF(Player p, Material material) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMinedF.row(uuid);

		int blocksMinedF = blocksmap.get(material);

		return blocksMinedF;
	}

	public static void addBlocksF(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMinedF.row(uuid);

		blocksmap.put(material, blocksmap.get(material) + value);

		return;

	}

	public static void removeBlocksF(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMinedF.row(uuid);

		blocksmap.put(material, blocksmap.get(material) - value);

		return;
	}

	public static void setupBlocksF(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> blocksmap = blocksMinedF.row(uuid);

		blocksmap.put(material, value);

		return;
	}

	public static void addBlockWhiteListF(Material material) {

		whitelistedblocksf.add(material);

		return;
	}

	public static void setLevelF(Player p, Material material, Integer value) {

		Map<Material, Integer> level = blockslevelF.row(p.getUniqueId());

		level.put(material, value);

		return;
	}

	public static Integer getLevelF(Player p, Material material) {

		Map<Material, Integer> level = blockslevelF.row(p.getUniqueId());

		int levelint = level.get(material);

		return levelint;
	}

	public static void setupLevelsF(Player p, Material material, Integer value) {

		UUID uuid = p.getUniqueId();

		Map<Material, Integer> level = blockslevelF.row(uuid);

		level.put(material, value);

		return;
	}

	public static Integer getblocksMinedFRequiredForLevelF(Player p, Material material) {

		FileConfiguration config = CollectionsMain.plugin.getConfig();

		int blocksrequired = 0;

		if (ForagingAPI.getLevelF(p, material) < 1) {

			blocksrequired = config.getInt("level1");

		}

		if (ForagingAPI.getLevelF(p, material) == 1) {

			blocksrequired = config.getInt("level2");

		}

		if (ForagingAPI.getLevelF(p, material) == 2) {

			blocksrequired = config.getInt("level3");

		}

		if (ForagingAPI.getLevelF(p, material) == 3) {

			blocksrequired = config.getInt("level4");

		}

		if (ForagingAPI.getLevelF(p, material) == 4) {

			blocksrequired = config.getInt("level5");

		}

		if (ForagingAPI.getLevelF(p, material) == 5) {

			blocksrequired = config.getInt("level6");

		}

		if (ForagingAPI.getLevelF(p, material) == 6) {

			blocksrequired = config.getInt("level7");

		}

		if (ForagingAPI.getLevelF(p, material) == 7) {

			blocksrequired = config.getInt("level8");

		}

		if (ForagingAPI.getLevelF(p, material) == 8) {

			blocksrequired = config.getInt("level9");

		}

		return blocksrequired;

	}

	public static void registerPlayerF(Player p) {

		Map<Material, Integer> blocksmap = blocksMinedF.row(p.getUniqueId());
		Map<Material, Integer> level = blockslevelF.row(p.getUniqueId());

		for (Material m : whitelistedblocksf) {

			if (blocksmap.get(m) == null) {
				setupBlocksF(p, m, 0);
			}

			if (level.get(m) == null) {
				setupLevelsF(p, m, 0);
			}

		}

	}

	public static void forceRegisterPlayer(Player p) {

		p.sendMessage("�3�lAll Of Your Foraging Collection Has Been Force Resetted!");

		for (Material m : whitelistedblocksf) {

			setupBlocksF(p, m, 0);
			setupLevelsF(p, m, 0);

		}

	}

	public static void collectionsdisable() {

	}
}
