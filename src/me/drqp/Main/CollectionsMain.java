package me.drqp.Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.drqp.Mining.API.CollectionsAPI;
import me.drqp.Mining.Commands.OpenCollectionsGUI;
import me.drqp.Mining.Commands.OpenSkillGUI;
import me.drqp.Mining.Events.BlockBreak;
import me.drqp.Mining.Events.InitialSetup;
import me.drqp.Mining.GUI.CollectionsGUI;
import me.drqp.Utils.UpdateChecker;
import me.drqp.skills.API.SkillsAPI;
import me.drqp.skills.Events.DamageEvent;
import me.drqp.skills.Events.JoinEvent;
import me.drqp.skills.GUI.MainGUI;

public class CollectionsMain extends JavaPlugin {
	private static ConfigManager cfgm;
	public static CollectionsMain plugin;
	public static boolean pluginon;

	public void onEnable() {

		CollectionsMain.plugin = this;

		UpdateChecker.serverstart();

		if (pluginon == true) {

			getServer().getPluginManager().registerEvents(new InitialSetup(), this);
			getServer().getPluginManager().registerEvents(new DamageEvent(), this);
			getServer().getPluginManager().registerEvents(new BlockBreak(), this);
			getServer().getPluginManager().registerEvents(new JoinEvent(), this);
			getServer().getPluginManager().registerEvents(new MainGUI(), this);
			getServer().getPluginManager().registerEvents(new CollectionsGUI(), this);

			loadConfigManager();

			InitialSetup.setup();

			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Loading Data......");

			initialloadmining();

			for (Player p : Bukkit.getOnlinePlayers()) {

				CollectionsAPI.registerPlayer(p);
				SkillsAPI.registerPoints(p);
				JoinEvent.setup(p);

			}

			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Loaded Sucesfully!");

			getConfig().options().copyDefaults(true);

			this.getCommand("skills").setExecutor(new OpenSkillGUI());
			this.getCommand("setpoints").setExecutor(new OpenSkillGUI());
			this.getCommand("collections").setExecutor(new OpenCollectionsGUI());

			saveConfig();

		} else {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.RED + "Plugin has been disabled!");
		}
	}

	public static void loadConfigManager() {

		cfgm = new ConfigManager();
		cfgm.setup();
		cfgm.savemining();
		cfgm.reloadmining();

	}

	public void onDisable() {

		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Saving Data!");

		savemining();

		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[Collections] " + ChatColor.GREEN + "Save Complete!");

	}

	public static void initialloadmining() {

		for (

		String rawData : cfgm.getmining().getStringList("blocksmineddata")) {

			String[] raw = rawData.split(":");

			CollectionsAPI.blocksMined.put(UUID.fromString(raw[0]), Material.valueOf(raw[1]), Integer.parseInt(raw[2]));

		}

		for (String rawData : cfgm.getmining().getStringList("levels")) {

			String[] raw = rawData.split(":");

			CollectionsAPI.blocklevel.put(UUID.fromString(raw[0]), Material.valueOf(raw[1]), Integer.parseInt(raw[2]));

		}

		for (String rawData : cfgm.getmining().getStringList("damage")) {

			String[] raw = rawData.split(":");

			SkillsAPI.damagemodifier.put(UUID.fromString(raw[0]), Integer.valueOf(raw[1]));

		}
		for (String rawData : cfgm.getmining().getStringList("health")) {

			String[] raw = rawData.split(":");

			SkillsAPI.healthmodifier.put(UUID.fromString(raw[0]), Integer.valueOf(raw[1]));

		}
		for (String rawData : cfgm.getmining().getStringList("damage")) {

			String[] raw = rawData.split(":");

			SkillsAPI.speedmodifier.put(UUID.fromString(raw[0]), Integer.valueOf(raw[1]));

		}

		for (String rawData : cfgm.getmining().getStringList("points")) {

			String[] raw = rawData.split(":");

			SkillsAPI.skillpoints.put(UUID.fromString(raw[0]), Integer.valueOf(raw[1]));

		}

		for (String rawData : cfgm.getmining().getStringList("mining")) {

			String[] raw = rawData.split(":");

			SkillsAPI.miningmodifier.put(UUID.fromString(raw[0]), Integer.valueOf(raw[1]));

		}

		String array = cfgm.getmining().getString("whitelistedblocks");

		array = array.substring(1, array.length() - 1);

		String[] arraysplit = array.split(", ");

		if (!(arraysplit.toString() == null)) {

			for (String s : arraysplit) {

				if (!(CollectionsAPI.whitelistedblocks.contains(Material.valueOf(s)))) {

					CollectionsAPI.whitelistedblocks.add(Material.valueOf(s));

				}

			}
		}
	}

	public static void savemining() {
		List<String> hashmapData = new ArrayList<String>();

		for (UUID uuid : CollectionsAPI.blocksMined.rowKeySet()) {

			Map<Material, Integer> blocksmap = CollectionsAPI.blocksMined.row(uuid);

			for (Material m : blocksmap.keySet()) {

				String data = uuid.toString() + ":" + m + ":" + blocksmap.get(m);
				hashmapData.add(data);

			}

		}

		cfgm.getmining().set("blocksmineddata", hashmapData);

		List<String> hashmapData2 = new ArrayList<String>();

		for (UUID uuid : CollectionsAPI.blocklevel.rowKeySet()) {

			Map<Material, Integer> level = CollectionsAPI.blocklevel.row(uuid);

			for (Material m : level.keySet()) {

				String data = uuid.toString() + ":" + m + ":" + level.get(m);
				hashmapData2.add(data);

			}

		}

		List<String> hashmapData3 = new ArrayList<String>();

		for (UUID uuid : SkillsAPI.damagemodifier.keySet()) {

			String data = uuid.toString() + ":" + SkillsAPI.damagemodifier.get(uuid);
			hashmapData3.add(data);

		}

		List<String> hashmapData4 = new ArrayList<String>();

		for (UUID uuid : SkillsAPI.healthmodifier.keySet()) {

			String data = uuid.toString() + ":" + SkillsAPI.healthmodifier.get(uuid);
			hashmapData4.add(data);

		}

		List<String> hashmapData5 = new ArrayList<String>();

		for (UUID uuid : SkillsAPI.speedmodifier.keySet()) {

			String data = uuid.toString() + ":" + SkillsAPI.speedmodifier.get(uuid);
			hashmapData5.add(data);

		}

		List<String> hashmapData6 = new ArrayList<String>();

		for (UUID uuid : SkillsAPI.skillpoints.keySet()) {

			String data = uuid.toString() + ":" + SkillsAPI.skillpoints.get(uuid);
			hashmapData6.add(data);

		}

		List<String> hashmapData7 = new ArrayList<String>();

		for (UUID uuid : SkillsAPI.miningmodifier.keySet()) {

			String data = uuid.toString() + ":" + SkillsAPI.miningmodifier.get(uuid);
			hashmapData7.add(data);

		}

		cfgm.getmining().set("blocksmined", hashmapData);
		cfgm.getmining().set("levels", hashmapData2);
		cfgm.getmining().set("damage", hashmapData3);
		cfgm.getmining().set("health", hashmapData4);
		cfgm.getmining().set("speed", hashmapData5);
		cfgm.getmining().set("points", hashmapData6);
		cfgm.getmining().set("mining", hashmapData7);
		cfgm.getmining().set("whitelistedblocks", CollectionsAPI.whitelistedblocks.toString());

		cfgm.savemining();
	}

}
